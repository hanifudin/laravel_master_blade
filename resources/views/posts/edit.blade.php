@extends('layouts.master')

@section('content')
    <div>
    <label for="">Edit Pos {{$post->id}}</label>
        <form role="form" action="/pertanyaan/{{$post->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="">judul</label> <br>
                <input type="text" name="judul" id="" value="{{$post->judul}}"><br>
                    <label for="pertanyaan-content">Pertanyaan</label>
                <textarea class="form-control" id="pertanyaan-content" name="isi" rows="3" placeholder="Apa yang mau ditanyakan?">{{$post->isi}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection