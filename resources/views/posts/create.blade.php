@extends('layouts.master')

@section('content')
    <div>
        <form role="form" action="/pertanyaan" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="">judul</label> <br>
                    <input type="text" name="judul" id=""> <br>
                    <label for="pertanyaan-content">Pertanyaan</label>
                    <textarea class="form-control" id="pertanyaan-content" name="isi" rows="3" placeholder="Apa yang mau ditanyakan?"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection