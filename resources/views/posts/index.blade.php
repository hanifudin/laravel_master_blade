@extends('layouts.master')

@section('content')

<div class="m-3">
  <h1>Daftar Pertanyaan</h1>
  <a href="/pertanyaan/create" class="btn btn-primary">Pertanyaan baru</a>
  <table class="table table-bordered mt-3">
      <thead>                  
          <tr>
              <th style="width: 10px">nomor</th>
              <th>Pertanyaan</th>
              <th>Jawaban</th>
              <th>action</th>
      </thead>
      <tbody>
          @foreach($posts as $key => $post)
              <tr>
                  <td>{{ $key + 1 }}</td>
                  <td>{{ $post->judul }}</td>
                  <td>{{ $post->isi }}</td>
                  <td>
                    <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm">show</a>
                    <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-info btn-sm">edit</a>
                    <form action="/pertanyaan/{{$post->id}}" method="POST">
                      @csrf
                      @method('Delete')
                      <input type="submit" name="" id="" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                  </td>             
              </tr>
          @endforeach
      </tbody>
  </table>
</div>
    
{{-- <table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">#</th>
        <th>Task</th>
        <th>Progress</th>
        <th style="width: 40px">Label</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1.</td>
        <td>Update software</td>
        <td>
          <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
          </div>
        </td>
        <td><span class="badge bg-danger">55%</span></td>
      </tr>
      <tr>
        <td>2.</td>
        <td>Clean database</td>
        <td>
          <div class="progress progress-xs">
            <div class="progress-bar bg-warning" style="width: 70%"></div>
          </div>
        </td>
        <td><span class="badge bg-warning">70%</span></td>
      </tr>
      <tr>
        <td>3.</td>
        <td>Cron job running</td>
        <td>
          <div class="progress progress-xs progress-striped active">
            <div class="progress-bar bg-primary" style="width: 30%"></div>
          </div>
        </td>
        <td><span class="badge bg-primary">30%</span></td>
      </tr>
      <tr>
        <td>4.</td>
        <td>Fix and squish bugs</td>
        <td>
          <div class="progress progress-xs progress-striped active">
            <div class="progress-bar bg-success" style="width: 90%"></div>
          </div>
        </td>
        <td><span class="badge bg-success">90%</span></td>
      </tr>
    </tbody>
  </table> --}}

  @endsection